const express = require('express')
const https = require('https')
const http = require('http')
const fs = require('fs')
const app = express()

const IS_PRODUCTION = false

const lex = require('greenlock-express').create({
  server: IS_PRODUCTION ? 'https://acme-v01.api.letsencrypt.org/directory' : 'staging',

  approveDomains: (opts, certs, cb) => {
    if (certs) {
      // change domain list here
      opts.domains = ['contenteditor-uat.cloud.jdplc.com'];
    } else {
      // change default email to accept agreement
      opts.email = 'Satej.Ramdin@jdplc.com';
      opts.agreeTos = true;
    }
    cb(null, { options: opts, certs });
  }
});

const middlewareWrapper = lex.middleware;

app.get('/', (req, res) => res.send('Hello World!'))

http.createServer(app).listen(5000)
https.createServer(lex.httpsOptions, middlewareWrapper(app)).listen(5001)
